#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>

// para cadenas de caracteres y string
using namespace std;

struct Archivo{
  char *nombre;
  int lineas;
  int palabras;
  int caracteres;
};

void *abrirArchivo(void *archivo){

  // variables para recorrer el archivo
  Archivo *str = (Archivo *)archivo;
  char c[2], *last_c;
  int lineas = 0, palabras = 0, caracteres = 0;
  bool despues_linea;

  //open devuelve un -1 cuando el archivo no se puede abrir
  int stream = open(str->nombre, O_RDONLY);

  if (stream == -1) {
    cout << "Error al abrir archivo" << endl;
    pthread_exit(0);
  }

  while(read(stream, c, 1)){
    // se lee el archivo caracter por caracter
    caracteres++;
    last_c = c; // ultimo caracter
    // si es que hay algo después de un salto de linea entonces antes habia una palabra
    if(despues_linea){
      palabras++;
      despues_linea = false; //reinicio de variable
    }
    // si hay salto de linea
    if (strchr("\n", c[0])){
      lineas++;
      despues_linea = true;
    }
    // palabras se separan mediante espacios
    if (strchr(" ", c[0])){
      palabras++;
    }
  }
    caracteres = caracteres - lineas; // retira saltos de linea
  /*
    Al salir de un archivo se cuenta un salto de linea pero si habia una
    palabra no se contó, por lo que la variable despues_linea revela el
    salto de linea final para contar la palabra
  */

  if(despues_linea){
    palabras++;
  }
  // si no se guarda un salto de linea al final entonces no contó una línea
  if(last_c!="\n"){
    lineas++;
    //palabras++;
  }

  cout << "El archivo " << str->nombre << " contiene:\n";
  cout << lineas << " líneas \n";
  cout << palabras << " palabras \n";
  cout << caracteres << " caracteres \n";

  close(stream);
  str->lineas = lineas;
  str->palabras = palabras;
  str->caracteres = caracteres;

  pthread_exit(0);

}

int main(int argc, char *argv[]) {
  // para contar entre todos los archivos
  int lineas_totales = 0, palabras_totales = 0, caracteres_totales = 0;
  int cantidad = argc-1;
  pthread_t threads[cantidad];

  Archivo files[cantidad];

  /*
    argc - 1 cuenta la cantidad de nombres de archivos dados
    argv[i+1] guarda y toma cada nombre de archivo dado
    por lo tanto es un ciclo que dura la cantidad de nombres dados
    enviando a la funcion cada archivo
 */
 unsigned ti;
 ti = clock();
 for(int i=0; i<cantidad; i++){
    files[i].nombre = argv[i+1];
    pthread_create(&threads[i], NULL, abrirArchivo, (void *)&files[i]);
    pthread_join(threads[i], NULL);
    lineas_totales += files[i].lineas;
    palabras_totales +=  files[i].palabras;
    caracteres_totales += files[i].caracteres;
  }


  cout << "Entre todos los archivos ingresados encontramos un total de: " <<
          lineas_totales << " líneas, " << palabras_totales << " palabras y "
          << caracteres_totales << " caracteres." << endl;

  unsigned tf;
  tf = clock();
  double tiempo;
  tiempo = (double(tf-ti)/CLOCKS_PER_SEC);
  cout << "El proceso demoró: " << tiempo << endl;

  return 0;
}
