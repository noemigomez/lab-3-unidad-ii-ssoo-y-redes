# Contador de líneas, palabras y caracteres de archivos

## Lab 3 - Unidad II - SSOO y redes

El programa mediante el uso de funciones abre una serie de archivos, dados los nombres por el usuario, y cuenta todas las líneas, palabras y caracteres que contiene. Contiene unso de hebras o hilos, lo cual permite que se realice la misma función en todos los archivos ocupando distintas partes de la CPU, realizando los cálculos al mismo tiempo.

## Comenzando 

Por terminal se entrega una serie de archivos los cuales se guardan como parametros de entrada y se van agregando y creando hebras con una función enviando cada archivo a una hebra. Estas hebras permiten la ejecucion de los programas al mismo tiempo compitiendo por la CPU. Recordar que hay caracteres que se cuentan como espacios, saltos de línea, tab, y más. 

## Prerrequisitos

### Sistema operativo

Para ejecutar el programa se necesita un sistema operativo Linux, de preferencia Ubuntu o Debian.

Siempre para instalar o correr programas se recomienda actualizar su máquina mediante el comando desde la terminal `sudo apt update && sudo apt upgrade`.

### Make

El programa cuenta con un archivo de compilación Make (Makefile). Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:

`sudo apt install make`

**En caso de no usar make**

Se puede ejecutar el programa sin make (revisar **Ejecutando las pruebas**).

## Ejecutando las pruebas

### Iniciar programa "compilar"

**Con make**

En la terminal situada en la carpeta con nuestros archivos se debe ejecutar el comando:

`make`

**Sin make**

En la terminar situada en la carpeta con nuestros archivos se debe ejecutar el siguiente comando:

`g++ programa_hebras.cpp -o programa_hebras -lpthread`

### Luego de compilar

Para ejecutar el programa una vez compilado se debe ejecutar el siguiente comando con parámetro de entrada:

`./programa_hebra {Archivos}`

Archivos puede corresponder a cualquier tipo de archivos que posean texto y estén dentro de la carpeta. Si se ingresan más archivos estos se separan por un espacio.

Sacando con ciclos y dependiendo de la cantidad de archivos de ingreso se generan estructuras por cada archivo, donde se guarda su nombre, la cantidad de palabras, lineas y caracteres. El nombre se guarda segun lo ingresado y los otros atributos se ingresan a medida que la función se ejecuta. 

Se envía creando una hebra la estructura, de esa manera en la hebra se guardan todos sus atributos una vez que cuente lo necesario. Se recorre el archivo caracter por caracter (por lo que el ciclo que la recorre aumenta el contador cada ve que entra), cuando encuentre un espacio entonces entre ellos hay palabras, se cuenta la palabra anterior, y cuando encuentre un salto de linea hay una linea. Al final de los archivos no suele haber salto de linea por lo que si el último caracter no es un salto de línea, se agrega la línea restante. De igual manera no se cuenta la palabra luego del salto de línea por lo que también se suma.

Una vez los atributos se agregan a la estructura y las hebras terminan todas sus procesos se suman las caracterisitcas para dar todos los atributos por archivo y de todos los archivos solicitados.

En caso de que el archivo no se pueda abrir el programa termina.

La salida forzosa del programa en cualquier parte de su ejecución es mediante `ctl+z`o `ctl+c`.

## Despliegue

La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux (señalado en Prerrequisitos). La rapidez de descarga depende únicamente del internet bajo el cual se ejecute. 

## Construido con
- Lenguaje c++: librería pthread.h, unistd.h, fcntl.h, string.h, iostream, sstream.


## Autor
- Noemí Gómez Rodríguez - nogomez19@alumnos.utalca.cl

## Comparar tiempo

Al final cuenta el tiempo de ejecución del programa que puede ser comparado por aquel hecho sin hebras. Revisar carpeta **Programa2**
