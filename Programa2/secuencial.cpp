#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>

// para cadenas de caracteres y string
using namespace std;

struct Archivo{
  char* nombre;
  int lineas;
  int palabras;
  int caracteres;
};

void *abrirArchivo(void *txt){

  // variables para recorrer el archivo
  Archivo *str = (Archivo *) txt;
  char c[2], *last_c;
  int lineas = 0, palabras = 0, caracteres = 0; // contadores
  bool despues_linea; // para contar palabra seguida de un salto de linea en lugar de un espacio

  // se abre archivo
  int stream = open (str->nombre, O_RDONLY);

  /*
  open devuelve un -1 cuando el archivo no se puede abrir
  Por ejemplo, cuando encuentra un archivo que no existe.
  */

  if (stream == -1) {
    cout << "Error al abrir archivo" << endl;
    pthread_exit(0);
  }else{
    while(read(stream, c, 1)){
      // se lee el archivo caracter por caracter
      caracteres++;
      last_c = c; // ultimo caracter
      // si es que hay algo después de un salto de linea entonces antes habia una palabra
      if(despues_linea){
        palabras++;
        despues_linea = false; //reinicio de variable
      }
      // si hay salto de linea
      if (strchr("\n", c[0])){
        lineas++;
        despues_linea = true;
      }
      // palabras se separan mediante espacios
      if (strchr(" ", c[0])){
        palabras++;
      }
    }
  }

    caracteres = caracteres - lineas; // retira saltos de linea
  /*
    Al salir de un archivo se cuenta un salto de linea pero si habia una
    palabra no se contó, por lo que la variable despues_linea revela el
    salto de linea final para contar la palabra
  */
  if(despues_linea){
    palabras++;
  }
  // si no se guarda un salto de linea al final entonces no contó una línea
  if(last_c!="\n"){
    lineas++;
  }

  cout << "El archivo " << str->nombre << " contiene:\n";
  cout << lineas << " líneas \n";
  cout << palabras << " palabras \n";
  cout << caracteres << " caracteres \n";

  close(stream);

  str->lineas = lineas;
  str->palabras = palabras;
  str->caracteres = caracteres;
  return 0;
}

int main(int argc, char *argv[]) {
  unsigned ti = clock();
  // para contar entre todos los archivos
  int lineas_totales = 0;
  int palabras_totales = 0;
  int caracteres_totales = 0;

  // Se mide el tiempo de cada proceso
  Archivo archivos[argc-1];

  /*
    argc - 1 cuenta la cantidad de nombres de archivos dados
    argv[i+1] guarda y toma cada nombre de archivo dado
    por lo tanto es un ciclo que dura la cantidad de nombres dados
    enviando a la funcion cada archivo
 */

  for(int i=0; i<argc - 1; i++){
    archivos[i].nombre = argv[i+1];
  }

  for(int i=0; i<argc - 1; i++){
    abrirArchivo((void *)&archivos[i]);

  }
  for(int i=0; i<argc - 1; i++){
    lineas_totales += archivos[i].lineas;
    palabras_totales += archivos[i].palabras;
    caracteres_totales += archivos[i].caracteres;
  }

  cout << "Entre todos los archivos ingresados encontramos un total de: ";
  cout << lineas_totales << " líneas, ";
  cout << palabras_totales << " palabras y ";
  cout << caracteres_totales << " caracteres." << endl;

  unsigned tf = clock();
  double tiempo = (double(tf-ti)/CLOCKS_PER_SEC);
  cout << "El proceso demoró: " << tiempo << endl;


  return 0;
}
